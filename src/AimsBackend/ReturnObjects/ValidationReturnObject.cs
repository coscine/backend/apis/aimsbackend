﻿namespace AimsBackend.ReturnObjects
{
    public class ValidationReturnObject
    {
        public bool Valid { get; set; }
    }
}
