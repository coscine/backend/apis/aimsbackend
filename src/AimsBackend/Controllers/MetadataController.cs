using AimsBackend.Model;
using AimsBackend.ReturnObjects;
using Microsoft.AspNetCore.Mvc;
using VDS.RDF;
using VDS.RDF.Query;
using VDS.RDF.Storage;
using VDS.RDF.Update;
using VDS.RDF.Writing;

namespace AimsBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MetadataController : ControllerBase
    {
        private readonly Coscine.Configuration.IConfiguration _configuration;
        private readonly string _graphPrefix;
        private readonly string _sparqlEndpoint;
        private readonly ILogger<MetadataController> _logger;

        public readonly SparqlRemoteUpdateEndpoint UpdateEndpoint;
        public readonly SparqlRemoteEndpoint QueryEndpoint;
        public readonly ReadWriteSparqlConnector ReadWriteSparqlConnector;

        public MetadataController(ILogger<MetadataController> logger)
        {
            _configuration = new Coscine.Configuration.EnvironmentConfiguration();
            _logger = logger;
            _graphPrefix = _configuration.GetString("GRAPH_PREFIX", "https://aims-projekt.de/metadata/");
            _sparqlEndpoint = _configuration.GetString("SPARQL_ENDPOINT", "http://vdb:8890/sparql");

            UpdateEndpoint = new SparqlRemoteUpdateEndpoint(new Uri(string.Format(_sparqlEndpoint)));
            QueryEndpoint = new SparqlRemoteEndpoint(new Uri(string.Format(_sparqlEndpoint)));
            ReadWriteSparqlConnector = new ReadWriteSparqlConnector(QueryEndpoint, UpdateEndpoint);
        }

        /// <summary>
        /// Gets metadata by a query and targetClass parameter and returns a list of text/turtle formatted metadata
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="predicate"></param>
        /// <param name="query"></param>
        /// <param name="targetClass"></param>
        /// <returns></returns>
        [HttpGet(Name = "GetMetadata")]
        public IEnumerable<string> Get(
            string? subject,
            string? predicate,
            string? query,
            Uri? targetClass)
        {
            return MetadataQuery(subject, predicate, query, null, targetClass);
        }

        /// <summary>
        /// Gets metadata by a query and targetClass parameter and returns a list of text/turtle formatted metadata
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="predicate"></param>
        /// <param name="query"></param>
        /// <param name="values"></param>
        /// <param name="targetClass"></param>
        /// <returns></returns>
        [HttpPost("/query", Name = "PostQueryMetadata")]
        public IEnumerable<string> Post(
            string? subject,
            string? predicate,
            string? query,
            List<PredicateObject>? values,
            Uri? targetClass)
        {
            return MetadataQuery(subject, predicate, query, values, targetClass);
        }

        private IEnumerable<string> MetadataQuery(string? subject, string? predicate, string? query, List<PredicateObject>? values, Uri? targetClass)
        {
            var valueFilter = "";
            var queryStringFilters = new List<Action<SparqlParameterizedString>>();
            if (values is not null)
            {
                var count = 1;
                foreach (var value in values)
                {
                    valueFilter +=
                        @$"FILTER(REGEX(str(?p), @pred{count})) .
                    FILTER(REGEX(str(?o), @obj{count})) .
                    ";

                    var localCount = count;
                    var localPred = value.Predicate;
                    var localObj = value.Object;

                    queryStringFilters.Add((query) => query.SetLiteral($"pred{localCount}", localPred));
                    queryStringFilters.Add((query) => query.SetLiteral($"obj{localCount}", localObj));
                    count++;
                }
            }

            var queryString = new SparqlParameterizedString
            {
                CommandText = @"SELECT DISTINCT ?g WHERE { 
                  GRAPH ?g { 
                    ?s ?p ?o .
                    " + (targetClass != null ? "?s a @targetClass ." : "") + @"
                    " + (subject != null ? "FILTER(REGEX(str(?s), @subject)) ." : "") + @"
                    " + (predicate != null ? "FILTER(REGEX(str(?p), @predicate)) ." : "") + @"
                    " + (query != null ? "FILTER(REGEX(str(?o), @query)) ." : "") + @"
                    " + valueFilter + @"
                  } .
                  FILTER(REGEX(str(?g), @graphPrefix)) .
                }"
            };
            if (targetClass != null)
            {
                queryString.SetUri("targetClass", targetClass);
            }
            if (subject != null)
            {
                queryString.SetLiteral("subject", subject);
            }
            if (predicate != null)
            {
                queryString.SetLiteral("predicate", predicate);
            }
            if (query != null)
            {
                queryString.SetLiteral("query", query);
            }
            foreach (var queryStringFilter in queryStringFilters)
            {
                queryStringFilter(queryString);
            }
            queryString.SetLiteral("graphPrefix", _graphPrefix);
            var resultSet = QueryEndpoint.QueryWithResultSet(queryString.ToString());

            var graphs = new List<string>();
            foreach (var result in resultSet)
            {
                graphs.Add(result.Value("g").ToString());
            }

            var metadata = new List<string>();
            var turtleWriter = new CompressingTurtleWriter();
            foreach (var graphUri in graphs)
            {
                var graph = new Graph();
                ReadWriteSparqlConnector.LoadGraph(graph, graphUri);
                metadata.Add(VDS.RDF.Writing.StringWriter.Write(graph, turtleWriter));
            }

            _logger.LogDebug($"Returned {metadata.Count} metadatasets");

            return metadata;
        }

        /// <summary>
        /// Creates a new graph with the metadata (application/n-triples) provided
        /// </summary>
        /// <param name="applicationProfile"></param>
        /// <param name="metadata"></param>
        [HttpPost(Name = "PostMetadata")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromQuery] string? applicationProfile, [FromBody] string? metadata)
        {
            var contentType = Request.Headers.ContentType;
            var mimeType = "application/n-triples";
            if (!string.IsNullOrWhiteSpace(contentType) && contentType != "application/json")
            {
                mimeType = contentType;
            }

            if (applicationProfile is null || metadata is null)
            {
                return BadRequest();
            }

            var graphUri = new Uri($"{_graphPrefix}{Guid.NewGuid()}");

            var graph = new Graph
            {
                BaseUri = graphUri
            };

            var parser = MimeTypesHelper.GetParser(mimeType);
            parser.Load(graph, new StringReader(metadata));

            if (graph.IsEmpty)
            {
                return BadRequest();
            }

            var validation = await ValidationModel.ValidateMetadata(applicationProfile, graph);
            if (!validation.Valid)
            {
                return BadRequest();
            }

            ReadWriteSparqlConnector.SaveGraph(graph);

            _logger.LogDebug($"Stored {graphUri}");

            return Created(graphUri.AbsoluteUri, metadata);
        }

        /// <summary>
        /// Validated metadata (application/n-triples) against a given application profile
        /// </summary>
        /// <param name="applicationProfile"></param>
        /// <param name="metadata"></param>
        [HttpPost("/validate", Name = "Validate")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ValidationReturnObject>> Validate([FromQuery] string? applicationProfile, [FromBody] string? metadata)
        {
            var contentType = Request.Headers.ContentType;
            var mimeType = "application/n-triples";
            if (!string.IsNullOrWhiteSpace(contentType) && contentType != "application/json")
            {
                mimeType = contentType;
            }

            if (applicationProfile is null || metadata is null)
            {
                return BadRequest();
            }

            var graphUri = new Uri($"{_graphPrefix}{Guid.NewGuid()}");

            var graph = new Graph
            {
                BaseUri = graphUri
            };

            var parser = MimeTypesHelper.GetParser(mimeType);
            parser.Load(graph, new StringReader(metadata));

            return Ok(await ValidationModel.ValidateMetadata(applicationProfile, graph));
        }
    }
}
