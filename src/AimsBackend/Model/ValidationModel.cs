﻿using AimsBackend.ReturnObjects;
using Newtonsoft.Json;
using System.Web;
using VDS.RDF;
using VDS.RDF.Shacl;

namespace AimsBackend.Model
{
    public class ValidationModel
    {
        public static string Endpoint { get; set; } = "https://pg4aims.ulb.tu-darmstadt.de/AIMS";
        public static readonly HttpClient HttpClient = new HttpClient();

        public static async Task<ValidationReturnObject> ValidateMetadata(string applicationProfile, IGraph metadata)
        {
            var fullApplicationProfileGraph = await RetrieveFullApplicationProfile(applicationProfile);

            var processor = new ShapesGraph(fullApplicationProfileGraph);
            var conforms = processor.Conforms(metadata);

            return new ValidationReturnObject() { Valid = conforms };
        }

        private static async Task<IGraph> RetrieveFullApplicationProfile(string applicationProfile)
        {
            var fullApplicationProfileGraph = new Graph();

            var visitedUris = new List<string>();
            var toQuery = new Queue<string>();
            toQuery.Enqueue(applicationProfile);
            while (toQuery.Count > 0)
            {
                var queryItem = toQuery.Dequeue();
                if (!visitedUris.Contains(queryItem))
                {
                    visitedUris.Add(queryItem);
                    var queryUrl = Endpoint + "/application-profiles/" + HttpUtility.UrlEncode(queryItem) + "?includeDefinition=true";
                    var result = await HttpClient.GetAsync(queryUrl);
                    var resultString = await result.Content.ReadAsStringAsync();
                    var applicationProfileObject = JsonConvert.DeserializeObject<ApplicationProfileObject>(resultString);
                    if (applicationProfileObject != null)
                    {
                        var parser = MimeTypesHelper.GetParser(applicationProfileObject.MimeType);
                        parser.Load(fullApplicationProfileGraph, new StringReader(applicationProfileObject.Definition));
                        var importObjects = fullApplicationProfileGraph.GetTriplesWithPredicate(new Uri("http://www.w3.org/2002/07/owl#imports"))
                            .Select((triple) => triple.Object)
                            .Where((obj) => obj is IUriNode)
                            .Select((obj) => (obj as IUriNode).Uri)
                            .Where((obj) => !visitedUris.Contains(obj.AbsoluteUri));
                        foreach (var importObject in importObjects)
                        {
                            toQuery.Enqueue(importObject.AbsoluteUri);
                        }
                    }
                }
            }

            return fullApplicationProfileGraph;
        }
    }
}
